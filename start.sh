#!/bin/bash
echo "START"

#alias php-fpm='php5-fpm'

#echo 'php5-fpm' > /usr/local/bin/php-fpm
#chmod +x /usr/local/bin/php-fpm

#exec php5-fpm --force-stderr --no-daemonize

echo "InstallPlugins"
curl -O `curl -i -s https://wordpress.org/plugins/post-types-order/ | egrep -o "https://downloads.wordpress.org/plugin/[^']+"`
unzip -o post-types-order.*.zip -d /usr/src/wordpress/wp-content/plugins
chown -R www-data:www-data /usr/src/wordpress/wp-content

nginx
docker-entrypoint.sh php5-fpm --force-stderr --nodaemonize

echo "END"
