FROM resin/rpi-raspbian:jessie
#FROM debian:jessie
MAINTAINER Florian Lefevre <trinita.andropos@gmail.com>

RUN apt-get update && apt-get install -y \
  curl unzip \
  nginx php5-cli php5-curl php5-gd php5-fpm php5-mysql \
  && rm -rf /var/lib/apt/lists/*

VOLUME /var/www/html
WORKDIR /var/www/html

ENV WORDPRESS_VERSION 4.6.1
ENV WORDPRESS_SHA1 027e065d30a64720624a7404a1820e6c6fff1202

# nginx config file
ADD ./nginx-site.conf /etc/nginx/sites-available/default

RUN set -x \
  && curl -o wordpress.tar.gz -fSL "https://wordpress.org/wordpress-${WORDPRESS_VERSION}.tar.gz" \
  && echo "$WORDPRESS_SHA1 *wordpress.tar.gz" | sha1sum -c - \
# upstream tarballs include ./wordpress/ so this gives us /usr/src/wordpress
  && tar -xzf wordpress.tar.gz -C /usr/src/ \
  && rm wordpress.tar.gz \
  && chown -R www-data:www-data /usr/src/wordpress

# Copy perfect CV plugin (from private)
COPY wp_cv_private/perfectcv /usr/src/wordpress/wp-content/themes/perfectcv
RUN mkdir /usr/src/wordpress/wp-content/uploads && chown www-data:www-data /usr/src/wordpress/wp-content/uploads -R

# Copy post-types-order plugin
#RUN set -x curl -O `curl -i -s https://wordpress.org/plugins/post-types-order/ | egrep -o "https://downloads.wordpress.org/plugin/[^']+"` \
#  && unzip -o post-types-order.*.zip -d /usr/src/wordpress/wp-content/plugins
#  && chown -R www-data:www-data /usr/src/wordpress

EXPOSE 80

ENV WORDPRESS_DB_NAME wp_cv

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
RUN ln -s usr/local/bin/docker-entrypoint.sh /entrypoint.sh # backwards compat

COPY start.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/start.sh

ENTRYPOINT ["start.sh"]
# Useless here i think
CMD php5-fpm --force-stderr --nodaemonize
