# docker-entrypoint.sh

GET from wordpress docker-library (5.6-fpm) :
https://github.com/docker-library/wordpress/tree/master/php5.6/fpm

We need to remove first test (if php-fpm) because php-fpm does not exist (we use php5-fpm).
php-fpm is available if we compile php directly

# Retrieve perfectcv from wp_cp_private
git submodule init
git submodule update
# Or, when cloning
git clone --recursive git@framagit.org:Trophalaxeur/wp_cv.git 


# Run mariadb on existing bdd (empty datadir folder to start from scratch)
$ docker run --name wp_cv_bdd -v /data/docker/wp_cv/bdd:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=test_pass -d hypriot/rpi-mysql

# Restore backup from script.sql
$ docker exec -i test_db mysql -uroot -ptest_pass < dump.sql

# Run mariadb on existing bdd (empty datadir folder to start from scratch)

# Build
$ docker build -t test_wp . 

# Run  Image
$ docker run -d --name test -v /my/own/uploaddir:/src/wordpress/wp-content/uploads --link test_db:mysql -e WORDPRESS_DB_PASSWORD=test_pass -p 80:80 test_wp

# Run into
$ docker exec -it test_wp bash


# Stop and remove 
$ docker stop test_wp; docker rm test_wp


